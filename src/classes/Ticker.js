export default class Ticker {
    constructor(rank=0) {
        this.rank = rank
        const state = {
            id: Math.random().toString(),
            name: this.defaultName(),
            unitsPerSecond: this.UPSBase(),
            level: 0,
            rank: rank,
            dateLastWithdrawn: Date.now(),
        }
        this.loadState(state)
    }

    loadState(state){
        this.id = state.id
        this.name = state.name
        this.unitsPerSecond = state.unitsPerSecond
        this.level = state.level
        this.rank = state.rank
        this.dateLastWithdrawn = state.dateLastWithdrawn
        return this
    }

    getState() {
        return {
            id: this.id,
            name: this.name,
            unitsPerSecond: this.unitsPerSecond,
            level: this.level,
            rank: this.rank,
            dateLastWithdrawn: this.dateLastWithdrawn,
        }
    }

    tick() {
        const now = Date.now()
        const diff = now - this.dateLastWithdrawn
        const rest = diff % 1000
        const diffFloor = diff - rest
        const tickAmount = diffFloor/1000 * this.unitsPerSecond
        this.dateLastWithdrawn = now - rest
        return tickAmount
    }

    upgradeCost() {
        const level = this.level 
        const rank = this.rank
        const rate = this.costRate(rank)
        const base = this.costBase(rank)
        return Math.floor(base * rate ** level)
    }

    static buyPrice(rank) {
        let price = 0
        switch(rank) {
            case 0:
                price = 20; break;
            case 1:
                price = 100; break;
            case 2:
                price = 489; break;
            case 3:
                price = 5010; break;
            case 4:
                price = 60234; break;
            default:
                price = (60234*2**rank); break;
        }
        return price
    }

    UPSBase() {
        const rank = this.rank
        let unitsPerSecond = 0
        switch(rank) {
            case 0:
                unitsPerSecond = 1.67; break;
            case 1:
                unitsPerSecond = 20; break;
            case 2:
                unitsPerSecond = 90; break;
            case 3:
                unitsPerSecond = 360; break;
            case 4:
                unitsPerSecond = 2160; break;
            default:
                unitsPerSecond = (2160*1.5**rank); break;
        }
        return unitsPerSecond
    }

    costBase() {
        const rank = this.rank
        let cost = 0  
        switch(rank) {
            case 0:
                cost = 16.7; break;
            case 1:
                cost = 200; break;
            case 2:
                cost = 900; break;
            case 3:
                cost = 3600; break;
            case 4:
                cost = 103680; break;
            default:
                cost = (103680*1.5**rank); break;
        }
        return cost
    }

    costRate() {
        const rank = this.rank
        let rate = 0  
        switch(rank) {
            case 0:
                rate = 1.14; break;
            case 1:
                rate = 1.15; break;
            case 2:
                rate = 1.14; break;
            case 3:
                rate = 1.13; break;
            case 4:
                rate = 1.12; break;
            defalt:
                rate = 1.12; break;
        }
        return rate
    }

    upgrade() {
        this.level += 1
        const level = this.level
        const rank = this.rank
        const base = this.UPSBase()
        const newUPS = (base * (level + 1)**(1.02))
        this.unitsPerSecond = newUPS
    }

    defaultName() {
        const rank = this.rank
        let name = `bot${rank}`  
        switch(rank) {
            case 0:
                name = `bot${rank}`; break;
            case 1:
                name = `bot${rank}`; break;
            case 2:
                name = `bot${rank}`; break;
            case 3:
                name = `bot${rank}`; break;
            case 4:
                name = `bot${rank}`; break;
            default:
                name = `bot${rank}`; break;
        }
        return name
    }
}

