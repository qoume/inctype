import Ticker from "./Ticker"
/**
 * Manages the logic of unit production.
 */
export default class UnitSystem {
    constructor(state) {
        this.__state__ = {
            units: 1000,
            lifetimeUnits: 1000,
            tickers: {},
            typerLevel: 0,
            typerItems: new Set(),
        }
        this.state = state || this.__state__
        this.loadState(this.state)
    }

    loadState(state) {
        this.lifetimeUnits = state.lifetimeUnits
        this.units = state.units
        this.tickers = this.initTickersState(state.tickers)
        this.typerLevel = state.typerLevel
        this.typerItems = state.typerItems
    }

    getState() {
        return {
            lifetimeUnits: this.lifetimeUnits,
            units: this.units,
            tickers: this.getTickersState(),
            typerLevel: this.typerLevel,
            typerItems: this.typerItems
        }
    }

    getTickersState() {
        const tickers = {}
        for (const ticker in this.tickers) {
            tickers[ticker] = this.tickers[ticker].getState()
        }
        return tickers
    }

    initTickersState(state) {
        const tickers = {}
        for (const ticker in state) {
            tickers[ticker] = (new Ticker()).loadState(state[ticker])
        }
        return tickers
    }

    getUnits(){
        return this.units
    }
    
    getLifetimeUnits(){
        return this.lifetimeUnits
    }

    //Returns true if it's possible to add units, else returns false.
    //For example if trying to spend more units than what is available.
    addUnits(n){
        if (this.units + n < 0)
            return false
        this.units += n
        this.lifetimeUnits += Math.max(0, n)
        return true
    }

    addTicker(rank) {
        const ticker = new Ticker(rank)
        this.tickers[ticker.id] = ticker
    }

    tick(id) {
        this.addUnits(this.tickers[id].tick())
    }

    tickAll() {
        for (const ticker in this.tickers){
            this.tick(ticker)
        }
    }

    typerDone(stats) {
        //TODO implement scoring
        this.addUnits(5)
    }
}
