import UnitSystem from './UnitSystem.js'
import ShopSystem from './ShopSystem.js'
//TODO Makes controller class for this class that can do things like coordinate interaction between different systems

/**
 * Main class for interacting with the game.
 * The react GUI interfaces with this class to update and get state.
 */
export default class User {
    constructor(id) {
//        if (typeof id === typeof this) {
//            //If being passed another User object
//            this = id
//            return this
//        }
        this.id = id
        this.__state__ = {
            //manages unit logic
            unitSystem: false,
            //manages shop logic
            shopSystem: false,
            //manges typing history logic
            historySystem: false,
            //manges statistics
            statsSystem: false,
            //manages settings
            settingsSystem: false,
        }

        //This state object is just for initialzing the different systems and for saving. 
        //When updates to game state are done while playing they will be done directly to the system objects.
        this.state = this.loadState() || this.__state__

        //Initialize all systems with user state.
        //If __state__ is used the systems will use their defaults.
        this.unitSystem = new UnitSystem(this.state.unitSystem)
        this.shopSystem = new ShopSystem(this.state.shopSystem)
        this.historySystem = false
        this.statsSystem = false
        this.settingsSystem = false

    }
    
    retrieveState() {
        return {
            unitSystem: this.unitSystem.getState(),
            shopSystem: this.shopSystem.getState(),
            historySystem: this.historySystem,
            statsSystem: this.statsSystem,
            settingsSystem: this.settingsSystem
        }
    }

    /**
     * Sets fields in this.state equal to saved state.
     */
    loadState() {

        const storeObject = localStorage.getItem(this.id)
        console.log("store object",this.id, storeObject)
        if (storeObject) {
            return JSON.parse(storeObject)
        } else {
            console.log("No stored user data.")
            return false
        }
    }

    /** 
     * Saves this.state to local storage
     */
    save() {
        console.log("Saved")
        this.state = this.retrieveState()
        localStorage.setItem(this.id, JSON.stringify(this.state))
    }
    
}
