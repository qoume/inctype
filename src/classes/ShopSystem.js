import itemData from "../shopItems.json"
import Ticker from "./Ticker"

export default class ShopSystem {
    constructor(state) {
        this.bought = new Set()
        this.unlocked = new Set()
        this.visible = new Set()
        this.ITEM_DATA = this.getStoreItems()
        console.log("shp",state)
        if (state) {
            this.setState(state)
        }
    }

    setState(state) {
        this.bought = new Set(state.bought)
        this.visible = new Set(state.visible)
        this.unlocked = new Set(state.unlocked)
    }

    getState() {
        const state = {
            bought: Array.from(this.bought),
            visible: Array.from(this.visible),
            unlocked: Array.from(this.unlocked),
        }
        return state
    }

    /**
     * Generates item objects based on states
     * TODO filter away all the bought items before doing other stuff.
     */
    getStoreItems() {
        const items = itemData
        for (const key in items) {
            items[key].id = key
            items[key].visible = this.isVisible(key)
            items[key].unlocked = this.isUnlocked(key)
            items[key].bought = this.isBought(key)
            if (items[key].body.type === "TICKER") {
                items[key].price = Ticker.buyPrice(items[key].body.rank)
            }
        }
        return items
    }

    buy(id) {
        this.addBought(id)
        this.removeVisible(id)
    }

    update(data) {
        for (const id in this.ITEM_DATA) {
            if (this.isBought(id)) continue;

            if (data.lifetimeUnits > this.ITEM_DATA[id].price/4) {
                this.addVisible(id)
            }

            if (data.lifetimeUnits > this.ITEM_DATA[id].price/2) {
                this.addUnlocked(id)
            }
        } 
    }

    /** */

    addBought(id) {
        this.bought.add(id) 
    }

    addVisible(id) {
        this.visible.add(id) 
    }

    addUnlocked(id) {
        this.unlocked.add(id) 
    }

    /** */

    removeBought(id) {
        this.bought.delete(id)
    }

    removeVisible(id) {
        this.visible.delete(id)
    }

    removeUnlocked(id) {
        this.unlocked.delete(id)
    }

    /** */

    isBought(id) {
        return this.bought.has(id) 
    }

    isVisible(id) {
        return this.visible.has(id)
    }

    isUnlocked(id) {
        return this.unlocked.has(id)
    }
}


