export default class Controller {
    constructor(user){
        this.updates = 0
        this.user = user
    }

    save() {
        this.user.save()
    }

    update() {
        this.updates += 1
        this.user.shopSystem.update({lifetimeUnits: this.user.unitSystem.lifetimeUnits})
        this.tickAll()
        if (this.updates % 10 == 0)
            this.save()
    }

    getUnits() {
        return this.user.unitSystem.getUnits()
    }

    setUnits(amount) {
        console.log("not implemented")
        return
    }
    addUnits(amount) {
        this.user.unitSystem.addUnits(amount)
    }

    /**
     * Called by app when an item is bought.
     * Handles all updates needed for the transaction.
     */
    buy(storeItem) {
        if (storeItem.body.type === "TICKER") {
            this.user.unitSystem.addTicker(storeItem.body.rank)   
            this.user.shopSystem.buy(storeItem.id)
            this.addUnits(-storeItem.price)
        } else {
            console.log("Not implemented.")
        }
    }

    /**
     * Get an object with the ticker objects
     */
    getTickers() {
        return {...this.user.unitSystem.tickers}
    }

    /**
     * Tick all tickers.
     */
    tickAll() {
        this.user.unitSystem.tickAll()
    }

    /**
     * When a typer is completed this functions is called and passed stats for the typing session
     */
    typerDone(stats) {
       this.user.unitSystem.typerDone(stats) 
       //TODO add to history
    }
}
