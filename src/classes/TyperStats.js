export default class TyperStats {
    begin(text) {
        this.WPM = 0
        this.accuracy = 0
        this.times = []
        this.wordsTyped = []
        this.timeIntervals = []
        this.grades = []
        this.words = text.split(" ")
        this.text = text
        this.times.push(new Date().getTime())
    }

    update(word) {
        this.times.push(new Date().getTime())
        this.wordsTyped.push(word[0])
        const len = this.times.length
        this.timeIntervals.push(this.times[len - 1] - this.times[len - 2])
        this.grades.push(word[1])
        this.accuracy = this.grades.reduce((prev, current) => prev + current) / Math.max(this.grades.length, 1) 
        const tDiff = (this.times[len - 1] - this.times[0])
        this.WPM = this.wordsTyped.length / (1/60 * tDiff / 1000)
        this.WPM *= this.accuracy
    }

    getState(){
        const state = {
            WPM: this.WPM,
            accuracy: this.accuracy,
            times: this.times,
            wordsTyped: this.wordsTyped,
            timeIntervals: this.timeIntervals,
            grades: this.grades,
            words: this.words,
            text: this.text,
            times: this.times,
            completionTime: Date.now(),
            readableWPM: this.readableWPM(),
            readableAccuracy: this.readableAccuracy(),
        }
        return state
    }

    readableWPM(WPM = this.WPM) {
        return Math.round(WPM)
    }

    readableAccuracy(accuracy = this.accuracy) {
        return `${Math.round(accuracy * 100)}%`
    }

}
