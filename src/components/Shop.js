import React, {useState, useEffect} from "react"

export default function Shop(props) {
    const items1 = Object.values(props.items)
        .filter(v => v.visible)
        .map((v, i) =>
        (<ShopItem 
            key={i} 
            units={props.units}
            name={v.name} 
            price={v.price}
            buy={() => props.buy(v)}
            unlocked={v.unlocked}/>))

    return (
        <div>
            <ol>{items1}</ol>
        </div>
    )
}

function ShopItem(props) {

    const output1 = !props.unlocked
        ? (
            <span>
                name: ???, price: ???, 
            </span>
        )
        : (
            <span>
                name: {props.name}, price: {props.price}, 
                {props.units >= props.price 
                ? (<button onClick={props.buy}>buy</button>)
                : (<button style={{"backgroundColor": "red"}}>buy</button>)}
            </span>
        )

    return (
        <li>
            {output1}
        </li>
    )
}

