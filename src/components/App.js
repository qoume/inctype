/**
 * TODO typer items
 * TODO stats system
 * TODO settings system
 * TODO history system
 * TODO getting text from somewhere interesting.
 */

import React, {useState, useEffect} from 'react'
import logo from './../logo.svg'

import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

import User from './../classes/User.js'
import Controller from'./../classes/Controller.js'
import Typer from './Typer.js'
import Ticker from './Ticker.js'
import Shop from './Shop.js'

import "./App.css"

const game = new Controller(new User('user42'))
const senLength = 5

const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
});

export default function App() {
    const [units, setUnits] = useState(game.getUnits())
    const [tickers, setTickers] = useState(game.getTickers())
    const [typerGoal, setTyperGoal] = useState(randomSentence(senLength))

    const readableUnits = (units) => {
        return Math.floor(units)
    }

    /**
     * trash
     */
    const classes = useStyles()

    /**
     * Creates an interval.
     */
    useEffect(() => {
        const updater = setInterval(() => {
            update()
        }, 500)
        return () => {
            clearInterval(updater)
        }
    }, [])

    /**
     * Startup stuff
     */
    useEffect(() => {
        console.log(game)
    }, [])

    const update = () => {
        game.update()
        setUnits(game.getUnits())
        setTickers(game.getTickers())
    }

    /**
     * Gets called when a typing session is completed
     */
    const typerDone = (typerStats) => {
        setTyperGoal(randomSentence(senLength))
        game.typerDone(typerStats)
        update()
    }

    /** 
     * Function passed to shop for buying items.
     */
    const buy = (storeItem) => {
        game.buy(storeItem)
        update()
    }

    /**
     * Shop component
     */
    const shop1 = (<Shop units={units} buy={buy} items={game.user.shopSystem.getStoreItems()}/>)


    /**
     * Typer component.
     */
    const typer1 = (<Typer level={1} typerDone={typerDone} text={typerGoal}/>)

    /**
     * Ticker components
     * TODO Move most of the logic through controller into ticker class
     */
    const tickerUpgrade = (ticker) => {
        const cost = ticker.upgradeCost()
        ticker.upgrade()
        game.addUnits(-cost)
        update()
    }

    const ticker1 = Object.values(tickers).map((v, i) => 
        <Grid container item justify="center" key={i}>
            <Ticker 
                units={units}
                id={v.id} 
                name={v.name}
                UPS={v.unitsPerSecond} 
                level={v.level}
                rank={v.rank}
                upgrade={() => tickerUpgrade(v)}
                upgradeCost={v.upgradeCost()}/>
        </Grid>)

    return (
        <Grid container spacing={2} direction="row" justify="center">
            <Grid item xs={3}>
                <Grid container item justify="center">
                    <Grid item>
                        <h2>units: {readableUnits(units)}</h2>
                    </Grid>
                </Grid>
                    <Grid container item justify="center">
                        <h1 item justify="center">Tickers: </h1>
                        {ticker1}
                    </Grid>
            </Grid>
            <Grid item xs={6}>
                <div>{typer1}</div>
                <h2>asdf1</h2>
                <h2>asdf2</h2>
                <h2>asdf3</h2>
                <h2>asdf4</h2>
                <h2>asdf5</h2>
            </Grid>
            <Grid item xs={3}>
                <Grid container item justify="center">
                    <Grid item>
                        {shop1}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

const randomSentence = (len) => {
    const dict = ["the", "of", "to", "and", "a", "in", "is", "it", "you", "that", "he", "was", "for", "on", "are", "with", "as", "I", "his", "they", "be", "at", "one", "have", "this", "from", "or", "had", "by", "not", "word", "but", "what", "some", "we", "can", "out", "other", "were", "all", "there", "when", "up", "use", "your", "how", "said", "an", "each", "she", "which", "do", "their", "time", "if", "will", "way", "about", "many", "then", "them", "write", "would", "like", "so", "these", "her", "long", "make", "thing", "see", "him", "two", "has", "look", "more", "day", "could", "go", "come", "did", "number", "sound", "no", "most", "people", "my", "over", "know", "water"]
    let sentence = []
    for (let i = 0; i < len; i++) {
        sentence.push(dict[Math.floor(Math.random() * dict.length)])
    }
    return sentence.join(" ")
}
