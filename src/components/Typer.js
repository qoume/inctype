import React, { useState, useEffect } from "react"
import Grid from '@material-ui/core/Grid';

import TyperText from "./TyperText"
import TyperStats from "../classes/TyperStats.js"

const typerStats = new TyperStats()

export default function Typer(props) {
    const [WPM, setWPM] = useState(0)
    const [accuracy, setAccuracy] = useState(1)
    const [typerKey, setTyperKey] = useState(0)

    useEffect(() => {
        typerNewText()
    }, [props.text])

    const typerNewText = () => {
        setTyperKey(prev => prev + 1)
    }

    const typerBegin = () => {
        typerStats.begin(props.text)
        setWPM(typerStats.WPM)
        setAccuracy(typerStats.accuracy)
    }

    const typerChange = (word) => {
        typerStats.update(word)
        setWPM(typerStats.WPM)
        setAccuracy(typerStats.accuracy)
    }

    const typerDone = () => {
        props.typerDone(typerStats.getState())
    }

    const stats1 = (<>
        <Grid item spacing={3}>
            <span>WPM {typerStats.readableWPM(WPM)}</span>
        </Grid>
        <Grid item spacing={3}>
            <span>acc {typerStats.readableAccuracy(accuracy)}</span>
        </Grid>
        <Grid item spacing={3}>
            <span>lvl {props.level}</span>
        </Grid>
    </>)

    return (
        <div className="component-outer typer-container">
            <Grid container item spacing={1}>
                {stats1}
            </Grid>
            <br></br>
            <Grid container item>
                <TyperText events={{
                    typerBegin: typerBegin,
                    typerDone: typerDone,
                    typerChange: typerChange
                }}
                    key={typerKey}
                    text={props.text}></TyperText>
            </Grid>
        </div>
    )
}
