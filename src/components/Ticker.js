import React from "react"

export default function Ticker(props) {
    const buyButton = (props.units >= props.upgradeCost)
        ? (
            <button onClick={props.upgrade}>upgrade</button>
        )
        : (
            <button style={{backgroundColor: "red"}}>upgrade</button> 
        )

    return (
        <div>
            <h1>qwer</h1>
            <p>name: {props.name}</p>
            <p>ups: {props.UPS.toFixed(2)}</p>
            <p>cost: {props.upgradeCost}</p>
            <p>level: {props.level}</p>
            <p>{buyButton}</p>
        </div>
    )
}
