import React, { useState, useRef, useEffect} from "react"
import "./Typer.css"

export default function TyperText(props) {
    const [text, setText] = useState("")
    const [completedText, setCompletedText] = useState([])
    const [remainingText, setRemainingText] = useState(props.text.trim().split(" "))
    const [inputRef, setInputFocus] = useFocus()
    const [hasBegun, setHasBegun] = useState(false)

    useEffect(() => {
        setInputFocus()
    }, [])

    useEffect(() => {
        if (hasBegun) props.events.typerBegin()
    }, [hasBegun])

    useEffect(() => {
        if (hasBegun) props.events.typerChange(completedText[completedText.length - 1])
        if (remainingText.length === 0) {
            props.events.typerDone()
        }
    }, [remainingText])

    const input = (e) => {
        let inputText = e.target.textContent
        const lastChar = inputText.charAt(inputText.length - 1)
        if (!hasBegun && !/\s/.test(lastChar)) setHasBegun(true)
        if (/\s/.test(lastChar)) {
            if (inputText.length === 1){
                e.target.textContent = ""
            } else {
                setCompletedText(prev => prev.concat([[
                    inputText.replace(/\s/g, ""),
                    text === remainingText[0]
                ]]))
                setText("")
                setRemainingText(prev => prev.slice(1))
                e.target.textContent = ""
            }
        } else {
            setText(inputText)
        }
    }

    const restOfWord = (currentInput, currentWord) => {
        if (!currentWord) return ""
        return currentWord.slice(currentInput.length)
    }

    const isCorrect = (currentInput, currentWord) => {
        if (!currentWord) return true
        if (currentInput.length > currentWord.length) return false
        for (let i = 0; i < currentInput.length; i++)
            if (currentInput[i] !== currentWord[i]) return false
        return true
    }

    const isCorrectToClass = (correct) => correct ? "typer-text-correct" : "typer-text-wrong"

    return (
        <div onClick={setInputFocus} className="typer-text-container">

            {completedText.map((word, i) => (
                <span className={`typer-text ${isCorrectToClass(word[1])}`}
                    key={"completed" + i}>
                    {word[0]}
                </span>
            ))}

            <span className="typer-input-container">

                <span ref={inputRef} 
                    onInput={input} 
                    className={`typer-text ${isCorrectToClass(isCorrect(text, remainingText[0]))}`}
                    contentEditable={remainingText.length > 0 ? "true" : "false"} 
                    tabIndex="1" 
                    autoCorrect="off" autoCapitalize="off" spellCheck="false" autoComplete="off"></span>
                <span className="typer-text typer-text-untyped typer-text-untouched">
                    {restOfWord(text, remainingText[0])}
                </span>
            </span>

            {remainingText.slice(1).map((word, i) => (
                <span className="typer-text typer-text-untyped" 
                    key={"remaining" + i}>
                    {word}
                </span>))}
        </div>
    )
}

const useFocus = () => {
    const ref = useRef(null)
    const setFocus = () => {
        ref.current &&  ref.current.focus()
    }
    return [ ref, setFocus ] 
}
